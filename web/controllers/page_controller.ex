defmodule Mbta.PageController do
  use Mbta.Web, :controller
  
  # Index is the only route for this project. Upon loading, download the schedule CSV,
  # parse the data and display in a HTML table
  def index(conn, _params) do
  	# Download the CSV file and store the response
	{:ok, response} = :httpc.request(:get, {'http://developer.mbta.com/lib/gtrtfs/Departures.csv', []}, [], [body_format: :binary])
	{{_, 200, 'OK'}, _headers, body} = response

	# Use ExCSV to parse the data to an array of objects, we will have table.headings and table.body
	{:ok, table} = body |> ExCsv.parse(headings: true) 
	
	# Delete the first column - it contains the current timestamp, we don't need to display this for every record
	headings = List.delete_at(table.headings, 0)
	
	# Get current time for EST and format (e.g. Saturday, 19 November, 9:16 PM)
	timestamp = Calendar.DateTime.now!("EST") |> Calendar.Strftime.strftime!("%A, %e %B, %l:%M %p")	
	
    render(conn, "index.html", departure_headings: headings, departure_body: table.body, current_time: timestamp)
  end
end